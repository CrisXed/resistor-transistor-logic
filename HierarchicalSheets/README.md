# Hierarchical Sheets

This is a placeholder KiCAD project I am using to design all the individual sheets which are used as hierarchical sheets
in the rest of the RTL repository.

## SPICE models
There are currently an additional 2 SPICE models, one for the NPN transistor and one for the PNP transistor.
These models were downloaded from the nexperia website.
