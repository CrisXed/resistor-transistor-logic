% !TEX encoding = UTF-8 Unicode
\documentclass[11pt, a4paper, draft]{article}
\usepackage[utf8x]{inputenc}
\usepackage{siunitx}
\usepackage[american, europeanvoltage, RPvoltages, cuteinductors, siunitx, betterproportions]{circuitikz}

\title{Resistor-Transistor Logic Design Considerations}
\author{Cristóvão Beirão da Cruz e Silva}

\ctikzloadstyle{romano}

\def\normalcoord(#1){coordinate(#1)}
\def\showcoord(#1){coordinate(#1) node[circle, red, draw, inner sep=1pt,
pin={[red, overlay, inner sep=0.5pt, font=\tiny, pin distance=0.1cm,
pin edge={red, overlay}]45:#1}](){}}
\let\coord=\normalcoord
\let\coord=\showcoord %Comment this once it is no longer draft

%\ctikzset{transistors/arrow pos=end}

\begin{document}

\maketitle

\begin{abstract}
I wanted to learn more about digital logic circuits and try designing some simple logic circuits with discrete components.
I decided to start with the resistor-transistor logic family, since it is often recommended as a good starting point.
The basic gates for the RTL family can easily be found online, however, the choice of the resistor values and other design considerations are not often explicitly stated.
In this document I am trying to summarise and keep note of all such decisions for my own logic family design.

If someone else ever stumbles upon this document and finds it useful, please let me know, it is always motivating to know others have found my work useful.
If there are any mistakes or weird/wrong assumptions, I would be glad to get feedback on that too.

Keep in mind, I am doing this as a hobby and these are not professional designs, as a result the designs provided here do not have any guarantee and may not be fit for purpose, if they are used anywhere, you are using them at your own risk.
As a result, I am making these designs available under the following terms (similar to those of the MIT license):
\begin{itemize}
\item The circuit designs are provided `as is', without any warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement.
\item In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the content in this document or its use
\item The origin of the content in this document must not be misrepresented and if it is used in a product an acknowledgement in the product documentation would be appreciated but is not required
\end{itemize}
\end{abstract}

\section{Resistor-Transistor Logic}
In the Resistor-Transistor logic family, the logic gates are composed of resistors and transistors.
Transistors are used as the switching elements while resistors are `used as input and output'\footnote{This sentence does not make much sense, but with the circuit diagrams below, it should become more explicit}.
The resistors provide input and output impedance to the circuit, and while in operation, since there will be currents circulating, this will result in voltage drops across the resistors.
These currents and voltage drops will be a large focus of the design considerations of the circuits since they will set the minimum (maximum) on (off) voltages, once a given transistor is chosen.

Before adventuring any further into this logic family, some assumptions must be introduced.
When working with logic families, we are referring to analog circuits whose input and output are being interpreted as digital values.
Since they are being interpreted as digital values some decisions are needed on how to interpret them.
For voltage based circuits, a logic 0, which can also be referred to as a boolean false or logic low, is typically assumed to be any voltage close to $0\,V$.
In contrast, a logic 1, which can also be referred to as a boolean true or logic high, is typically assumed to be any voltage close to $V_{CC}$.
The ranges of voltages for `any voltage close' is part of the design considerations of the logic family, explored further below.
For now it is sufficient to assume that a logic 0 is $0\,V$ and that a logic 1 is some other positive voltage.
These assumptions are not generally true, since they depend on several factors such as the logic family and the design considerations, for example the NIM standard is a current based standard which defines a logic 0 as no current and a logic 1 as a negative current.

\begin{figure}[!htb]
  \centering
  \begin{circuitikz}[scale=0.9, transform shape, use fpu reciprocal,]
    \draw (0,0) node[npn](Q){};
    \draw (Q.B) -- ++(-0.5,0) to[R, l=$R_B$] ++(-1,0) -- ++(-0.5,0) node[left]{$v_{in}$};
    \draw (Q.E) -- ++(0,-0.5) node[ground]{};
    \draw (Q.C) to[short, *-] ++(0,0.5) to[R, l=$R_C$] ++(0,1) -- ++(0,0.5) node[vcc]{$V_{CC}$};
    \draw (Q.C) -- ++(1,0) node [right] {$v_{out}$};
    \path (Q.center) \coord(center) (Q.B) \coord(B) (Q.C) \coord(C) (Q.E) \coord(E);
  \end{circuitikz}
  \caption{Typical RTL inverter gate}
  \label{fig:RTLInvertTypical}
\end{figure}

The gate which is typically used to introduce the RTL family is the inverter, shown in Fig.~\ref{fig:RTLInvertTypical}, also known as the ´Not Gate' since it implements the boolean not operator.
If we assume that the transistor functions as a switch in this circuit, it is quite intuitive to understand how the circuit functions:
\begin{itemize}
  \item When the input ($v_{in}$) is logic low, i.e. it is $0\,V$, the switch will be open and $v_{out}$ will take the value $V_{CC}$ since there is no current flow
  \item When the input ($v_{in}$) is logic high, i.e. it is $V_{CC}$, the switch will be closed and $v_{out}$ will take the value $0\,V$ since the switch is shorting it to ground
\end{itemize}
It can now be understood that this circuit takes an input and inverts it on the output (thus the name Inverter), i.e. it converts a logic low into a logic high and a logic high into a logic low.
Even though this analysis is quite primitive since it does not consider the full spectrum of behaviour of the transistor nor the currents through the resistors, which necessarily arise when connecting multiple gates together, it permits to obtain a base understanding of the circuit which we will attempt to maintain through a judicious choice of the resistors.

The traditional RTL approach for implementing a ``Not OR Gate'' (NOR Gate) is to duplicate the base resistor and have one for each input, see Fig.~\ref{fig:RTLNORTraditional}.
This also often requires the addition of the resistor $R_D$ in order to provide enough design flexibility to get the desired behaviour of the gate.
It is then only a question of carefully balancing the resistor values such that a high enough voltage is present at the transistor base when either or both inputs are high.

\begin{figure}[!htb]
  \centering
  \begin{circuitikz}[scale=1, transform shape, use fpu reciprocal,]
    \draw (0,0) node[npn](Q){};
    \draw (Q.B) -- ++(-0.5,0) node[](D){} -- ++(-1,0) node[](T){} -- ++(0,0.8) -- ++(-0.5,0) to[R, l=$R_{B1}$] ++(-1,0) -- ++(-0.5,0) node[left]{$v_{in1}$};
    \draw (D) to[short, *-] ++(0,-1) to[R, l=$R_D$] ++(0,-1) -- ++(0,-0.5) node[ground](GND){};
    \draw (T) to[short, *-] ++(0,-0.8) -- ++(-0.5,0) to[R, l=$R_{B2}$] ++(-1,0) -- ++(-0.5,0) node[left]{$v_{in2}$};
    \draw (Q.E) -- (GND-|Q.E) node[ground]{};
    \draw (Q.C) to[short, *-] ++(0,0.5) to[R, l=$R_C$] ++(0,1) -- ++(0,0.5) node[vcc]{$V_{CC}$};
    \draw (Q.C) -- ++(1,0) node [right] {$v_{out}$};
    \path (Q.center) \coord(center) (Q.B) \coord(B) (Q.C) \coord(C) (Q.E) \coord(E);
  \end{circuitikz}
  \caption{Traditional RTL NOR gate}
  \label{fig:RTLNORTraditional}
\end{figure}

While this design does work, its history originates from a time where a transistor was an expensive component, so the gates were designed to have as few transistors as possible.
This is no longer the case, and we are not under the same constraint so we will take a different approach when designing the more complex gates.
By taking a different approach, explained below, we will also improve the isolation between the two inputs of the gate.
Keep in mind however that due to its historical significance, it could be argued that Fig.~\ref{fig:RTLNORTraditional} represents the `true RTL NOR Gate', whatever that means and whatever weight that statement carries.

For our design, the `Not Gate'  can be thought of as a basic building block and if two are connected together with a common $R_C$, see Fig.~\ref{fig:RTLNORSimple}, we can then analyse the circuit by considering the transistor a switch:
\begin{itemize}
  \item When both inputs are low, both switches are open, so no current flows through $R_C$. Thus, $v_{out}$ is $V_{CC}$
  \item When one (or both) input(s) are high, the corresponding switch(es) are closed. Thus, the output is shorted to $0\,V$. 
\end{itemize}
As a result, we can conclude that this circuit implements a `NOR gate', as was desired.

\begin{figure}[!htb]
  \centering
  \begin{circuitikz}[scale=1, transform shape, use fpu reciprocal,]
    \draw (0,0) node[npn](Q1){};
    \draw (Q1.center) ++(1,0) node[npn, xscale=-1](Q2){};
    \draw (Q1.B) -- ++(-0.5,0) to[R, l=$R_{B1}$] ++(-1,0) to[short, -o] ++(-0.5,0) node[left]{$v_{in1}$};
    \draw (Q2.B) -- ++(0.5,0) to[R, l=$R_{B2}$] ++(1,0) to[short, -o] ++(0.5,0) node[](VIn2){} node[right]{$v_{in2}$};
    \draw (Q1.C) to[short, -*] ++(0.5,0) node [](T){} -- (Q2.C);
    \draw (T) to[short, -*] ++(0,1) node[](O){} -- ++(0,0.5) to[R, l=$R_C$] ++(0, 1) -- ++(0,0.5) node[vcc]{$V_{CC}$};
    \draw (O) to[short, -o] (O-|VIn2) node[right]{$v_{out}$};
    \draw (Q1.E) -- ++(0,-0.5) node[ground](GND){};
    \draw (Q2.E) -- (GND-|Q2.E) node[ground]{};
    \path (Q1.center) \coord(center) (Q1.B) \coord(B) (Q1.C) \coord(C) (Q1.E) \coord(E);
    \path (Q2.center) \coord(center) (Q2.B) \coord(B) (Q2.C) \coord(C) (Q2.E) \coord(E);
  \end{circuitikz}
  \caption{Simple RTL NOR Gate}
  \label{fig:RTLNORSimple}
\end{figure}


\end{document}